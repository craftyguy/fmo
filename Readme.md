# fmo - Find My Orphans

This is intended to help you locate packages you have installed in Arch Linux that are orphans in AUR. Once you find them, [you should maintain them!](https://bbs.archlinux.org/viewtopic.php?id=50869)

Since this searches for AUR orphans, it assumes you have [auracle](https://github.com/falconindy/auracle) installed. Auracle is a new/improved AUR helper.

## Installation on Arch Linux

[Install package from AUR](https://aur.archlinux.org/packages/fmo/)
